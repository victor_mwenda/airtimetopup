package com.marvik.apps.lib.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class SettingsUtils implements Prefs {

	Context mContext;
	SharedPreferences prefs;
	Editor editor;

	@Override
	public boolean autoTopup() {
		// TODO Auto-generated method stub
		return prefs.getBoolean(Utils.AUTO_TOPUP, false);
	}

	public SettingsUtils(Context context) {
		this.mContext = context;
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
		editor = prefs.edit();
	}

	@Override
	public boolean backupTopups() {
		// TODO Auto-generated method stub

		return prefs.getBoolean(Utils.BACKUP_TOPUPS, false);
	}

	@Override
	public boolean showSplash() {
		// TODO Auto-generated method stub
		return prefs.getBoolean(Utils.SHOW_SPLASH, false);
	}

	@Override
	public boolean showNotification() {
		// TODO Auto-generated method stub
		return prefs.getBoolean(Utils.SHOW_NOTIFICATION, false);
	}

	@Override
	public float rating() {
		// TODO Auto-generated method stub
		return prefs.getFloat(Utils.RATING, 0f);
	}

	@Override
	public int mobileNetwork() {
		// TODO Auto-generated method stub
		return prefs.getInt(Utils.MOBILE_NETWORK, 0);
	}

	@Override
	public String productKey() {
		// TODO Auto-generated method stub
		return prefs.getString(Utils.PRODUCT_KEY, "");
	}

	@Override
	public void setAutoTopup(boolean autoTopup) {
		// TODO Auto-generated method stub
		editor.putBoolean(Utils.AUTO_TOPUP, autoTopup);
		editor.commit();
	}

	@Override
	public void setBackupTopups(boolean backupTopups) {
		// TODO Auto-generated method stub
		editor.putBoolean(Utils.BACKUP_TOPUPS, backupTopups);
		editor.commit();
	}

	@Override
	public void setShowSplash(boolean showSplash) {
		// TODO Auto-generated method stub
		editor.putBoolean(Utils.SHOW_SPLASH, showSplash);
		editor.commit();
	}

	@Override
	public void setShowNotification(boolean showNotification) {
		// TODO Auto-generated method stub
		editor.putBoolean(Utils.SHOW_NOTIFICATION, showNotification);
		editor.commit();
	}

	@Override
	public void setRating(float rating) {
		// TODO Auto-generated method stub
		editor.putFloat(Utils.RATING, rating);
		editor.commit();
	}

	@Override
	public void setMobileNetwork(int mobileNetwork) {
		// TODO Auto-generated method stub
		editor.putInt(Utils.MOBILE_NETWORK, mobileNetwork);
		editor.commit();
	}

	@Override
	public void setProductKey(String productKey) {
		// TODO Auto-generated method stub
		editor.putString(Utils.PRODUCT_KEY, productKey);
		editor.commit();
	}

	@Override
	public boolean hideActionBar() {
		// TODO Auto-generated method stub
		return prefs.getBoolean(Utils.HIDE_ACTIONBAR, false);
	}

	@Override
	public void setActionBarHidden(boolean hideActionBar) {
		// TODO Auto-generated method stub
		editor.putBoolean(Utils.HIDE_ACTIONBAR, hideActionBar);
		editor.commit();
	}

	@Override
	public int keyboard() {
		// TODO Auto-generated method stub
		return prefs.getInt(Utils.KEYBOARD, 0);
	}

	@Override
	public void setKeyboard(int keyboard) {
		// TODO Auto-generated method stub
		editor.putInt(Utils.KEYBOARD, keyboard);
		editor.commit();
	}

}
