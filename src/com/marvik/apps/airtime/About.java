package com.marvik.apps.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.marvik.apps.lib.airtime.ActivityUI;
import com.marvik.apps.lib.airtime.SettingsUtils;
import com.marvik.apps.lib.airtime.Utils;

public class About extends ActivityUI {
	SettingsUtils settingsUtils;
	WebView wvAbout;
	TelephonyManager tm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		try {
			initialise();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void initialise() throws Exception {
		// TODO Auto-generated method stub
		settingsUtils = new SettingsUtils(About.this);
		RelativeLayout actionBar = (RelativeLayout) findViewById(R.id.action_bar);
		if (settingsUtils.hideActionBar()) {
			actionBar.setVisibility(RelativeLayout.GONE);
		}

		wvAbout = (WebView) findViewById(R.id.about_webView);
		wvAbout.setWebViewClient(new AboutWebClient());

		tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

		if (tm.getDataState() == TelephonyManager.DATA_CONNECTED) {
			wvAbout.loadUrl("http://www.merusongs.com/repository/victormwenda/victormwenda/apps/airtime_topup/files/about.html");
		} else {
			sendBroadcast(new Intent(Utils.ERROR_NO_INTERNET_ACCESS));
			Log.i("About", "No -net");
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (tm.getDataState() == TelephonyManager.DATA_CONNECTED) {
			wvAbout.loadUrl("http://www.merusongs.com/repository/victormwenda/victormwenda/apps/airtime_topup/files/about.html");
		} else {
			sendBroadcast(new Intent(Utils.ERROR_NO_INTERNET_ACCESS));
			Log.i("About", "No -net");
		}

	}

	class AboutWebClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			// TODO Auto-generated method stub
			view.loadUrl(url);
			return super.shouldOverrideUrlLoading(view, url);
		}
	}

}
