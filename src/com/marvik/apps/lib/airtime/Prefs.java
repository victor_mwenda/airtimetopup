package com.marvik.apps.lib.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

public interface Prefs {
	public boolean autoTopup();

	public boolean backupTopups();

	public boolean hideActionBar();

	public int keyboard();

	public boolean showSplash();

	public boolean showNotification();

	public float rating();

	public int mobileNetwork();

	public String productKey();

	public void setBackupTopups(boolean backupTopups);

	public void setShowSplash(boolean showSplash);

	public void setShowNotification(boolean showNotification);

	public void setRating(float rating);

	public void setMobileNetwork(int mobileNetwork);

	public void setProductKey(String productKey);

	public void setAutoTopup(boolean autoTopup);

	public void setActionBarHidden(boolean hideActionBar);

	public void setKeyboard(int keyboard);
}
