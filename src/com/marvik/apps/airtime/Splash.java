package com.marvik.apps.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.marvik.apps.lib.airtime.SettingsUtils;
import com.marvik.apps.lib.airtime.Utils;

public class Splash extends Activity {

	SettingsUtils settingsUtils;
Utils utils;
TextView tvDeveloper;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		initialise();
		
	}

	private void initialise() {
		// TODO Auto-generated method stub
		settingsUtils = new SettingsUtils(Splash.this);
		utils = new Utils(Splash.this);
		tvDeveloper = (TextView)findViewById(R.id.splash_textView_developer);
		tvDeveloper.setText("Marvik Apps 2014");
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(Splash.this);
		boolean firstRun = prefs.getBoolean(Utils.FIRST_RUN, true);
		
		if (firstRun) {
			Editor editor = prefs.edit();
			editor.putBoolean(Utils.ADD_SHORTCUT, true);
			editor.commit();
			startActivity(new Intent(getApplicationContext(), Register.class));
			finish();
		} else {
			if (settingsUtils.showSplash()) {

				new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							Thread.sleep(2000);
							utils.startActivity(MainActivity.class, Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
							finish();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}).start();
			} else {
				startActivity(new Intent(getApplicationContext(), MainActivity.class));
				finish();
			}
		}
	}
}
