package com.marvik.apps.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.marvik.apps.lib.airtime.ActivityUI;
import com.marvik.apps.lib.airtime.SettingsUtils;
import com.marvik.apps.lib.airtime.Utils;

public class Settings extends ActivityUI implements OnCheckedChangeListener {
	SettingsUtils settingsUtils;
	CheckBox cbAutoTopup, cbBackTopups, cbNotification, cbSplashScreen,
			cbActionBar;
	RatingBar rbRating;
	EditText etProductKey;
	Spinner spNetwork, spKeyboard;
	LinearLayout llPremium;
	ImageView ivProductKey;
	TextView tvRateDeveloper;
	Utils utils;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		initialise();
		readPreferences();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub

		super.onPause();
		savePreferences();

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		savePreferences();

		finish();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		readPreferences();

	}

	private void initialise() {
		// TODO Auto-generated method stub
		settingsUtils = new SettingsUtils(Settings.this);
		utils = new Utils(Settings.this);

		RelativeLayout actionBar = (RelativeLayout) findViewById(R.id.action_bar);
		if (settingsUtils.hideActionBar())
			actionBar.setVisibility(RelativeLayout.GONE);

		cbAutoTopup = (CheckBox) findViewById(R.id.settings_checkBox_autotopup);
		cbBackTopups = (CheckBox) findViewById(R.id.settings_checkBox_backup);
		cbNotification = (CheckBox) findViewById(R.id.settings_checkBox_notification);
		cbNotification.setOnCheckedChangeListener(new NotificationCBListener());
		cbSplashScreen = (CheckBox) findViewById(R.id.settings_checkBox_splash);
		cbActionBar = (CheckBox) findViewById(R.id.settings_checkBox_actionbar);
		etProductKey = (EditText) findViewById(R.id.settings_editText_product_key);
		etProductKey.setText(getResources().getString(R.string.product_key));
		etProductKey.addTextChangedListener(new ProductKeyWatcher());
		spNetwork = (Spinner) findViewById(R.id.settings_spinner_network);
		spKeyboard = (Spinner) findViewById(R.id.settings_spinner_keyboardtype);
		rbRating = (RatingBar) findViewById(R.id.settings_ratingBar_rating);
		rbRating.setOnRatingBarChangeListener(new DeveloperRatingListener());
		ivProductKey = (ImageView) findViewById(R.id.settings_imageView_productKey);
		tvRateDeveloper = (TextView) findViewById(R.id.settings_textView_rateDeveloper);
		llPremium = (LinearLayout) findViewById(R.id.settings_linealLayout_premium);
		setPremiumFeaturesEnabled(false);

		if (settingsUtils.productKey().equals(getString(R.string.product_key))) {
			setPremiumFeaturesEnabled(true);
			Bitmap bm = BitmapFactory.decodeResource(getResources(),
					android.R.drawable.btn_star_big_on);
			etProductKey.setTextColor(Color.WHITE);
			etProductKey.setBackgroundColor(Color.WHITE);
			ivProductKey.setImageBitmap(bm);
		}

	//Important clicks that a user needs guidance
		cbAutoTopup.setOnCheckedChangeListener(this);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if(buttonView.getId()==R.id.settings_checkBox_autotopup){
			if(cbAutoTopup.isChecked()){
			Dialog autotopupDialog = new Dialog(Settings.this);
			autotopupDialog.setTitle("Autotopup");
			TextView topupTextView = new TextView(Settings.this);
			topupTextView.setPadding(10, 10, 10, 10);
			topupTextView.setBackgroundColor(Color.parseColor("#FFE4B5"));
			topupTextView.setTextColor(Color.parseColor("#D2691E"));
			topupTextView.setText("If you enable this feature, the topup" +
					" my phone and other phone buttons will nolonger be " +
					"available and airtime topups shall be automatic to your phone or the specified phone after inputing valid topup keys");
			autotopupDialog.setContentView(topupTextView);
			autotopupDialog.setCancelable(true);
			autotopupDialog.setCanceledOnTouchOutside(true);
			autotopupDialog.show();
			}
		}
	}
	public void setPremiumFeaturesEnabled(boolean premiumFeatures) {
		cbAutoTopup.setEnabled(premiumFeatures);
		cbBackTopups.setEnabled(premiumFeatures);
		cbNotification.setEnabled(premiumFeatures);
		cbSplashScreen.setEnabled(premiumFeatures);
		cbActionBar.setEnabled(premiumFeatures);
		rbRating.setEnabled(premiumFeatures);
		tvRateDeveloper.setEnabled(premiumFeatures);
		spKeyboard.setEnabled(premiumFeatures);
		}

	private void savePreferences() {
		// TODO Auto-generated method stub

		if (cbAutoTopup.isChecked()) {
			settingsUtils.setAutoTopup(true);
		} else {
			settingsUtils.setAutoTopup(false);
		}

		if (cbActionBar.isChecked()) {
			settingsUtils.setActionBarHidden(true);
		} else {
			settingsUtils.setActionBarHidden(false);
		}

		if (cbBackTopups.isChecked()) {
			settingsUtils.setBackupTopups(true);
		} else {
			settingsUtils.setBackupTopups(false);
		}

		settingsUtils.setMobileNetwork(spNetwork.getSelectedItemPosition());
		settingsUtils.setKeyboard(spKeyboard.getSelectedItemPosition());
		settingsUtils.setProductKey(etProductKey.getText().toString());

		settingsUtils.setRating(rbRating.getRating());

		if (cbNotification.isChecked()) {
			settingsUtils.setShowNotification(true);
		} else {
			settingsUtils.setShowNotification(false);
		}

		if (cbSplashScreen.isChecked()) {
			settingsUtils.setShowSplash(true);
		} else {
			settingsUtils.setShowSplash(false);
		}

	}

	private void readPreferences() {
		// TODO Auto-generated method stub
		cbAutoTopup.setChecked(settingsUtils.autoTopup());
		cbBackTopups.setChecked(settingsUtils.backupTopups());
		cbNotification.setChecked(settingsUtils.showNotification());
		cbSplashScreen.setChecked(settingsUtils.showSplash());
		cbActionBar.setChecked(settingsUtils.hideActionBar());
		rbRating.setRating(settingsUtils.rating());
		etProductKey.setText(settingsUtils.productKey());
		spNetwork.setSelection(settingsUtils.mobileNetwork());
		spKeyboard.setSelection(settingsUtils.keyboard());
	}

	class ProductKeyWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

			if (etProductKey.getText().toString()
					.equals(getString(R.string.product_key))) {
				setPremiumFeaturesEnabled(true);
				Bitmap bm = BitmapFactory.decodeResource(getResources(),
						android.R.drawable.btn_star_big_on);
				ivProductKey.setImageBitmap(bm);
				etProductKey.setTextColor(Color.WHITE);
				etProductKey.setBackgroundColor(Color.WHITE);
			} else {
				setPremiumFeaturesEnabled(false);
				Bitmap bm = BitmapFactory.decodeResource(getResources(),
						android.R.drawable.btn_star_big_off);
				ivProductKey.setImageBitmap(bm);
				etProductKey.setTextColor(Color.BLACK);
				etProductKey.setBackgroundColor(Color.WHITE);
			}
		}

	}

	class NotificationCBListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			if (isChecked) {

				PendingIntent contentIntent = PendingIntent.getActivity(
						Settings.this, 0, new Intent(getApplicationContext(),
								MainActivity.class),
						Intent.FLAG_ACTIVITY_NEW_TASK);
				String contentTitle = "Airtime Topup";
				String contentText = "Click to launch the main app";
				int icon = R.drawable.ic_launcher;
				long when = System.currentTimeMillis();
				int notificationID = 1993;
				int flags = Notification.FLAG_NO_CLEAR;
				String tickerText = "You can now quick launch airtime topup from the notification bar";
				utils.showNotification(contentIntent, contentTitle,
						contentText, icon, when, notificationID, tickerText,
						flags);
			} else {

				PendingIntent contentIntent = PendingIntent.getActivity(
						Settings.this, 0, new Intent(getApplicationContext(),
								MainActivity.class),
						Intent.FLAG_ACTIVITY_NEW_TASK);
				String contentTitle = "Airtime Topup";
				String contentText = "Click to launch the main app";
				int icon = R.drawable.ic_launcher;
				long when = System.currentTimeMillis();
				int notificationID = 1993;
				String tickerText = "You can now quick launch airtime topup from the notification bar";
				int flags = Notification.FLAG_NO_CLEAR;
				utils.cancelNotification(contentIntent, contentTitle,
						contentText, icon, when, notificationID, tickerText,
						flags);
			}
		}

	}

	class DeveloperRatingListener implements OnRatingBarChangeListener {

		@Override
		public void onRatingChanged(RatingBar ratingBar, float rating,
				boolean fromUser) {
			// TODO Auto-generated method stub
			Log.i("Rating changed", "Saving changes");
			new AsyncTask<Void, Void, Integer>() {

				@Override
				protected void onPreExecute() {
					// TODO Auto-generated method stub
					sendBroadcast(new Intent(Utils.ACTION_UPLOAD_SETTINGS));
					super.onPreExecute();
				}

				@Override
				protected Integer doInBackground(Void... params) {
					// TODO Auto-generated method stub
					try {
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(
								"http://www.merusongs.com/victormwenda/apps/airtime_topup/files/settings.php");
						List<NameValuePair> ratingForm = new ArrayList<NameValuePair>();
						String[] network = getResources().getStringArray(
								R.array.mobile_networks);
						SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Settings.this);
						
						ratingForm.add(new BasicNameValuePair("email",prefs.getString(Utils.USER_EMAIL, "")));
						ratingForm.add(new BasicNameValuePair("rating", String
								.valueOf(rbRating.getRating())));
						ratingForm.add(new BasicNameValuePair("network",
								network[settingsUtils.mobileNetwork()]));
						ratingForm.add(new BasicNameValuePair("product_key",
								etProductKey.getText().toString()));
						ratingForm.add(new BasicNameValuePair("backup_topups",
								String.valueOf(settingsUtils.backupTopups())));
						ratingForm
								.add(new BasicNameValuePair("notification",
										String.valueOf(settingsUtils
												.showNotification())));
						ratingForm.add(new BasicNameValuePair("show_splash",
								String.valueOf(settingsUtils.showSplash())));
						ratingForm.add(new BasicNameValuePair("auto_topup",
								String.valueOf(settingsUtils.autoTopup())));
						ratingForm.add(new BasicNameValuePair("hide_actionbar",
								String.valueOf(settingsUtils.hideActionBar())));
						post.setEntity(new UrlEncodedFormEntity(ratingForm));
						HttpResponse response = client.execute(post);

						return response.getStatusLine().getStatusCode();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return Utils.MARVIK_HTTP_ERROR;
				}

				@Override
				protected void onPostExecute(Integer result) {
					// TODO Auto-generated method stub
					switch (result) {
					case 200:
						Log.i("response", "success");
						sendBroadcast(new Intent(
								Utils.SETTINGS_UPLOAD_SUCCESSFUL));
						break;
					case Utils.MARVIK_HTTP_ERROR:
						Log.i("response", "failed");
						sendBroadcast(new Intent(
								Utils.SETTINGS_UPLOAD_FAILED_INTERNET_ERROR));
						break;
					default:
						Log.i("response", "unknown");
						sendBroadcast(new Intent(Utils.SETTINGS_UPLOAD_FAILED));
						break;
					}
					super.onPostExecute(result);
				}

			}.execute();

		}

	}

	
}
