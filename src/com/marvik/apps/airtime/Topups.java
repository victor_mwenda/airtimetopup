package com.marvik.apps.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import java.util.Date;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;


import com.marvik.apps.lib.airtime.ActivityUI;
import com.marvik.apps.lib.airtime.SettingsUtils;
import com.marvik.apps.lib.airtime.Utils;

public class Topups extends ActivityUI implements  OnItemLongClickListener {
	SettingsUtils settingsUtils;
	ListView lvTopups;
	View retopupUI;
	Button btCancel ; 
	Button btRetopup ; 
	Spinner spNetwork ; 
	EditText etPhonenumber ; 
	EditText etTopupkeys ; 
	Dialog retopup;
	Utils utils;
	int currentNetwork;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.topups);
		initialise();
		
	}

	@SuppressWarnings("deprecation")
	private void initialise() {
		// TODO Auto-generated method stub
		settingsUtils = new SettingsUtils(Topups.this);
		utils = new Utils(Topups.this);
		RelativeLayout actionBar = (RelativeLayout) findViewById(R.id.action_bar);
		if (settingsUtils.hideActionBar()) {
			actionBar.setVisibility(RelativeLayout.GONE);
		}

		lvTopups = (ListView) findViewById(R.id.topups_listView_topups);
		lvTopups.setOnItemLongClickListener(this);
		
		String[] from = new String[] { Utils.COL_NETWORK, Utils.COL_TIME,
				Utils.COL_TOPUP_KEYS, Utils.COL_TOPUP_NUMBER,
				Utils.COL_TOPUP_TYPE };
		int[] to = new int[] { R.id.top_ups_ui_textView_mobile_network,
				R.id.top_ups_ui_textView_time,
				R.id.top_ups_ui_textView_topup_keys,
				R.id.top_ups_ui_textView_topup_type,
				R.id.top_ups_ui_textView_topup_status };
		Cursor cursor = getContentResolver().query(
				Utils.CONTENT_URI,
				new String[] { Utils.COL_ID, Utils.COL_NETWORK, Utils.COL_TIME,
						Utils.COL_TOPUP_KEYS, Utils.COL_TOPUP_NUMBER,
						Utils.COL_TOPUP_TYPE }, null, null, null);
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(Topups.this,
				R.layout.topups_ui, cursor, from, to);
		lvTopups.setAdapter(adapter);
		startManagingCursor(cursor);
	}

	
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		
		Log.i("Topups List - Long click "+(position+1), "Long click on item "+(position+1));
		
		TopupDatabase db = new TopupDatabase(Topups.this);
		SQLiteDatabase sql = db.getReadableDatabase();
		Cursor c = sql.query(Utils.TABLE_NAME,
				null,
				Utils.COL_ID +"=" +(position +1),
				null, null, null, null);
		String network = null;
		String topupkeys = null;
		String phonenumber = null;
		
		while(c.moveToFirst()){
			network = c.getString(c.getColumnIndex(Utils.COL_NETWORK));
			topupkeys = c.getString(c.getColumnIndex(Utils.COL_TOPUP_KEYS));
			phonenumber = c.getString(c.getColumnIndex(Utils.COL_TOPUP_NUMBER));
			
			Log.i("Topups", "Reading the entry \n"
			+"Network :: "+network+"\n"
			+"Topup Keys :: "+topupkeys +"\n"
			+"Phonenumber :: "+phonenumber +"\n");
			break;
		}
		db.close();
		sql.close();
		retopup = new Dialog(Topups.this);
		retopup.setTitle("My topups");
		
		retopupUI = getLayoutInflater().inflate(R.layout.retopup, null, false);
		btCancel = (Button) retopupUI.findViewById(R.id.retopup_button_cancel);
		btRetopup = (Button) retopupUI.findViewById(R.id.retopup_button_retopup);
		spNetwork = (Spinner) retopupUI.findViewById(R.id.retopup_spinner_network);
		etPhonenumber = (EditText) retopupUI.findViewById(R.id.retopup_editText_phn_no);
		etTopupkeys = (EditText) retopupUI.findViewById(R.id.retopup_editText_topup_keys);
		
		btCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				retopup.cancel();
			}
		});

		btRetopup.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				retopup.cancel();
				if(etPhonenumber.isShown()){
				retopup(spNetwork.getSelectedItemPosition(),utils.getString(etTopupkeys),utils.getString(etPhonenumber));
				}else{
					retopup(spNetwork.getSelectedItemPosition(),utils.getString(etTopupkeys));	
				}
				}
		});
		
		spNetwork.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				String [] networks = getResources().getStringArray(R.array.mobile_networks);
				Log.i("Retopup Network", "Selected " +networks[position]);
				//utils.toast("Network change failed", Toast.LENGTH_LONG);
				spNetwork.setSelection(currentNetwork);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				Log.i("Retopup Network", "Nothing Selected ");
				
			}
		});
		
		retopup.setCancelable(false);
		retopup.setCanceledOnTouchOutside(false);
		retopup.setContentView(retopupUI);
		retopup.show();
		if(retopup.isShowing()){
			etPhonenumber.setText("");
			etTopupkeys.setText(topupkeys);
			etPhonenumber.setVisibility(EditText.GONE);
			
			if(!phonenumber.equals(Utils.MY_PHONE)){
				etPhonenumber.setVisibility(EditText.VISIBLE);
				etPhonenumber.setText(phonenumber);
			}
			String[] mobiNetwork = getResources().getStringArray(R.array.mobile_networks);
			for (int i = 0; i < mobiNetwork.length; i++) {
				Log.i("Querying networks ", "Checking . . ."+mobiNetwork[i]);
				if (network.equals(mobiNetwork[i])) {
					spNetwork.setSelection(i);
					currentNetwork = spNetwork.getSelectedItemPosition();
					utils.toast("The selected network ["+mobiNetwork[i]+"] cannot be changed!", Toast.LENGTH_LONG);
					Log.i("Querying networks ", "Setting network to"+mobiNetwork[i]);
					
				}
			}
		}
		
		Log.i("Topups", "Retop up");
		return false;
		
	}
	private void backupTopup() {
		
		String sType=Utils.TOPED_UP;
		String[] aNetwork = getResources().getStringArray(R.array.mobile_networks);
		String sNumber="";
		if (etPhonenumber.isShown()) {
			sNumber = new String(etPhonenumber.getText().toString());
		}
		String sKeys = etTopupkeys.getText().toString();
		String sTime = new Date().toString();

		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(Utils.COL_NETWORK, aNetwork[spNetwork.getSelectedItemPosition()]);
		cv.put(Utils.COL_TIME, sTime);
		cv.put(Utils.COL_TOPUP_KEYS, sKeys);
		cv.put(Utils.COL_TOPUP_NUMBER, sNumber);
		cv.put(Utils.COL_TOPUP_TYPE, sType);
		getContentResolver().insert(Utils.CONTENT_URI, cv);

		Log.i("Retopup", "saving data to topups provider");
	}
	private void retopup(int network, String topupkeys,
			String phonenumber) {
		// TODO Auto-generated method stub
		
		Log.i("Retopup", "Network:"+network +"\n Topupkeys : " +topupkeys+"\nPhonenumber : "+phonenumber);
		if(settingsUtils.backupTopups()){
			backupTopup();
		}
		switch(network){
		case 0:
			Log.i ("Retop up","Airtel");
			startActivity(new Intent(Intent.ACTION_CALL,
					Uri.parse("tel:*122*" + topupkeys +"*" +phonenumber +"%23")));
			break;
		//Orange
		case 1:
			Log.i ("Retop up","Orange");
			startActivity(new Intent(Intent.ACTION_CALL,
					Uri.parse("tel:*130*" + topupkeys +"*" +phonenumber + "%23")));
			break;
		//Safaricom
		case 2:
			Log.i ("Retop up","Safaricom");
			startActivity(new Intent(Intent.ACTION_CALL,
					Uri.parse("tel:*141*" + topupkeys +"*" +phonenumber + "%23")));
			break;
		//Yu-Essar
		case 3:
			Log.i ("Retop up","Yu-Essar");
			startActivity(new Intent(Intent.ACTION_CALL,
					Uri.parse("tel:*130*" + topupkeys +"*" +phonenumber + "%23")));
			break;
		}
	}
	private void retopup(int network, String topupkeys) {
		// TODO Auto-generated method stub
		Log.i("Retopup", "Network:"+network +"\n Topupkeys : " +topupkeys+"\n");
		if(settingsUtils.backupTopups()){
			backupTopup();
		}
		switch(network){
		//Airtel
		case 0:
			Log.i ("Retop up","Airtel");
			startActivity(new Intent(Intent.ACTION_CALL,
					Uri.parse("tel:*122*" + topupkeys + "%23")));
			break;
		//Orange
		case 1:
			Log.i ("Retop up","Orange");
			startActivity(new Intent(Intent.ACTION_CALL,
					Uri.parse("tel:*130*" + topupkeys + "%23")));
			break;
		//Safaricom
		case 2:
			Log.i ("Retop up","Safaricom");
			startActivity(new Intent(Intent.ACTION_CALL,
					Uri.parse("tel:*141*" + topupkeys + "%23")));
			break;
		//Yu-Essar
		case 3:
			Log.i ("Retop up","Yu-Essar");
			startActivity(new Intent(Intent.ACTION_CALL,
					Uri.parse("tel:*130*" + topupkeys + "%23")));
			break;
		}
	}
}
