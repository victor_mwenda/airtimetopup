package com.marvik.apps.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import java.util.ArrayList;
import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.marvik.apps.lib.airtime.SettingsUtils;
import com.marvik.apps.lib.airtime.Utils;

public class Register extends Activity {

	SettingsUtils settingsUtils;
	EditText etName, etEmail;
	Button btRegister;
	TelephonyManager tm;
	int status;
	Utils utils;
	ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		initialise();

		createDesktopShortcut();
		sendMarvikBroadcast();
		addUserEmail();
		
	}

	private void initialise() {
		// TODO Auto-generated method stub
		utils = new Utils(Register.this);
		tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		settingsUtils = new SettingsUtils(Register.this);

		RelativeLayout actionBar = (RelativeLayout) findViewById(R.id.action_bar);
		if (settingsUtils.hideActionBar())
			actionBar.setVisibility(RelativeLayout.GONE);

		etName = (EditText) findViewById(R.id.register_editText_name);
		etEmail = (EditText) findViewById(R.id.register_editText_email);
		btRegister = (Button) findViewById(R.id.register_button_register);
		btRegister.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					if (!etName.getText().toString().equals("")
							&& !etEmail.getText().toString().equals("")) {
						Pattern pEmail = Patterns.EMAIL_ADDRESS;
						if (pEmail.matcher(etEmail.getText().toString())
								.matches()) {
							saveUsernameAndEmailInPreferences(etName.getText()
									.toString(), etEmail.getText().toString());

							new RegisterThread().execute();

						} else
							etEmail.setError("Invalid email address");
					} else {
						if (etName.getText().toString().equals(""))
							etName.setError("Provide your name");
						if (etEmail.getText().toString().equals(""))
							etEmail.setError("Provide email address");
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	private class RegisterThread extends AsyncTask<Void, Void, Integer> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			dialog = new ProgressDialog(Register.this);
			dialog.setTitle("User registration console");
			dialog.setMessage("Registering the user\n \""
					+ etName.getText().toString() + "\" \n "
					+ "using the email address \n \"" 
					+ etEmail.getText().toString() + " \"");
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected Integer doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			sendBroadcast(new Intent(Utils.REGISTRATION_SUCCESSFUL));
			finish();
			
			return register(etName.getText().toString(), etEmail.getText()
					.toString());
		}

		@Override
		protected void onPostExecute(Integer registerResult) {
			// TODO Auto-generated method stub
			
			
			dialog.dismiss();
			switch (registerResult) {
			case 200:
				new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						sendBroadcast(new Intent(Utils.REGISTRATION_SUCCESSFUL));
						finish();	
					}
				}.run();
				break;
			case Utils.MARVIK_HTTP_ERROR:
				sendBroadcast(new Intent(Utils.ERROR_NO_INTERNET_ACCESS));
				finish();
				break;

			default:
				sendBroadcast(new Intent(Utils.REGISTRATION_FAILED));
				
				break;
			}
			super.onPostExecute(registerResult);
		}
	}

	@SuppressWarnings("deprecation")
	private int register(final String name, final String email) {
		// TODO Auto-generated method stub

/*		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(
					"http://www.merusongs.com/victormwenda/apps/airtime_topup/files/register.php");
			List<NameValuePair> regForm = new ArrayList<NameValuePair>();
			regForm.add(new BasicNameValuePair("name", name));
			regForm.add(new BasicNameValuePair("email", email));
			regForm.add(new BasicNameValuePair("sdk_ver", Build.VERSION.SDK));
			regForm.add(new BasicNameValuePair("imei", tm.getDeviceId()));
			regForm.add(new BasicNameValuePair("phone_no", tm.getLine1Number()));
			regForm.add(new BasicNameValuePair("network", tm
					.getNetworkOperator()));
			post.setEntity(new UrlEncodedFormEntity(regForm));
			HttpResponse response = client.execute(post);
			status = response.getStatusLine().getStatusCode();
			return status;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Utils.MARVIK_HTTP_ERROR;*/
		return 200;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(Register.this);
		Editor editor = prefs.edit();
		editor.putBoolean(Utils.REGISTRED, false);
		editor.putBoolean(Utils.FIRST_RUN, true);
		editor.commit();
		finish();
	}

	private void sendMarvikBroadcast() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Utils.MARVIK_APP_FIRST_RUN);
		Bundle bundle = new Bundle();
		bundle.putString(Utils.MARVIK_APPS_EXTRA_APP_NAME, "Airtime Topup");
		bundle.putString(Utils.MARVIK_APPS_EXTRA_PACKAGE_NAME, "com.marvik.apps.airtime");
		bundle.putString(Utils.MARVIK_APPS_EXTRA_DEVELOPER_NAME,
				"Victor Mwenda");
		bundle.putString(Utils.MARVIK_APPS_EXTRA_DOWNLOAD_LINK,
				"http://www.merusongs.com/MarvikApps/AirtimeTopup.apk");
		// bundle.putString("", "");
		intent.putExtras(bundle);
		sendBroadcast(intent);
	}

	private void createDesktopShortcut() {
		// TODO Auto-generated method stub
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(Register.this);
		if (prefs.getBoolean(Utils.ADD_SHORTCUT, true)) {
			Intent shortCut = new Intent(getApplicationContext(), Splash.class);
			shortCut.setAction(Intent.ACTION_MAIN);

			Intent i = new Intent(Intent.ACTION_CREATE_SHORTCUT);
			i.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Airtime Topup");
			i.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortCut);
			i.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
					Intent.ShortcutIconResource.fromContext(
							getApplicationContext(), R.drawable.ic_launcher));
			i.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
			sendBroadcast(i);
			
			Editor editor = prefs.edit();
			editor.putBoolean(Utils.ADD_SHORTCUT, false);
			editor.commit();
		}
	}

	private void addUserEmail() throws NullPointerException {
		// TODO Auto-generated method stub
		AccountManager man = (AccountManager) getSystemService(ACCOUNT_SERVICE);
		Account[] account = man.getAccounts();
		String emailAccount = "";
		Pattern emailPattern = Patterns.EMAIL_ADDRESS;
		final ArrayList<String> emailsList = new ArrayList<String>();
		for (Account mAccount : account) {

			if (emailPattern.matcher(mAccount.name).matches())
				emailsList.add(mAccount.name);

		}
		if (emailsList.size() == 1) {
			emailAccount = emailsList.get(0);
			etEmail.setText(emailAccount);
		}
		if (emailsList.size() > 1) {
			final Dialog dialog = new Dialog(Register.this);
			final ListView listView = new ListView(Register.this);
			listView.setAdapter(new ArrayAdapter<String>(Register.this,
					R.layout.custom_list, R.id.custom_list_textView, emailsList));
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					etEmail.setText(emailsList.get(arg2));
					dialog.dismiss();
				}
			});
			dialog.setTitle("Choose your email account");
			dialog.setContentView(listView);
			dialog.setCancelable(false);
			dialog.setCanceledOnTouchOutside(false);
			dialog.show();
			

		}
	}

	protected void saveUsernameAndEmailInPreferences(String username,
			String email) {
		// TODO Auto-generated method stub

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(Register.this);

		Editor editor = prefs.edit();
		editor.putString(Utils.USER_NAME, username);
		editor.putString(Utils.USER_EMAIL, email);

		editor.commit();
	}
}
