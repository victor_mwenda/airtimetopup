package com.marvik.apps.lib.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.marvik.apps.airtime.MainActivity;
import com.marvik.apps.airtime.R;

public class MarvikReceiver extends BroadcastReceiver {
	Utils utils;
	SettingsUtils settingsUtils;
	Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		this.context = context;
		utils = new Utils(context);
		settingsUtils = new SettingsUtils(context);
		if (intent.getAction().equals(Utils.MARVIK_APP_FIRST_RUN)) {
			try {
				Bundle bundle = intent.getExtras();
				String appname = bundle
						.getString(Utils.MARVIK_APPS_EXTRA_APP_NAME);
				String developer = bundle
						.getString(Utils.MARVIK_APPS_EXTRA_DEVELOPER_NAME);
				@SuppressWarnings("unused")
				String downloadLink = bundle
						.getString(Utils.MARVIK_APPS_EXTRA_DOWNLOAD_LINK);
				String packageName = bundle.getString(Utils.MARVIK_APPS_EXTRA_PACKAGE_NAME);
				String string = "A new app called " + appname
						+ " developed by " + developer
						+ " has just been first launched in your system";
				if (packageName.equalsIgnoreCase("com.marvik.apps.airtime")) {
					Log.i("New marvik app", "App first run");
				} else {
					utils.toast(string, Toast.LENGTH_LONG);
				}

			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}

		if (intent.getAction().equals(Utils.REGISTRATION_SUCCESSFUL)) {

			if (registerUser()) {
				utils.toast("Registration successful", Toast.LENGTH_SHORT);
				Log.i("registration", "successful");
			}
		}
		if (intent.getAction().equals(Utils.REGISTRATION_FAILED)) {
			Log.i("registration", "failed");
			utils.toast("Registration failed", Toast.LENGTH_SHORT);
		}
		if (intent.getAction().equals(Utils.BUGREPORT_SENT)) {
			utils.toast("Bugreport sent", Toast.LENGTH_SHORT);
			Log.i("bugreport", "sent");
		}
		if (intent.getAction().equals(Utils.BUGREPORT_FAILED)) {
			utils.toast("Bugreport sending failed", Toast.LENGTH_SHORT);
			Log.i("bug-report", "sending failed");
		}
		if (intent.getAction().equals(Utils.ERROR_NO_INTERNET_ACCESS)) {
			Log.i("error", "no internet connection");
			utils.toast(
					"No Internet access . . . navigating to Internet settings",
					Toast.LENGTH_LONG);
			utils.startActivity(
					android.provider.Settings.ACTION_WIRELESS_SETTINGS,
					Intent.FLAG_ACTIVITY_NEW_TASK);

		}
		if (intent.getAction().equals(Utils.ACTION_UPLOAD_SETTINGS)) {
			// utils.toast("Uploading settings ", Toast.LENGTH_SHORT);
			Log.i("settings", "uploading");
		}
		if (intent.getAction().equals(Utils.SETTINGS_UPLOAD_FAILED)) {
			// utils.toast("Settings upload failed", Toast.LENGTH_SHORT);
			Log.i("settings", "upload failed");
		}
		if (intent.getAction().equals(
				Utils.SETTINGS_UPLOAD_FAILED_INTERNET_ERROR)) {
			// utils.toast("Settings upload failed - no Internet",Toast.LENGTH_SHORT);
			Log.i("settings", "not-uploaded - no internet connection");
		}
		if (intent.getAction().equals(Utils.SETTINGS_UPLOAD_SUCCESSFUL)) {
			// utils.toast("Settings upload successful", Toast.LENGTH_SHORT);
			Log.i("settings", "uploaded");
		}
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			if (settingsUtils.showNotification()) {
				//Make sure a notification will be restored
				if (restoreNotification())
					Log.i("boot complete", "showing notification");
				else restoreNotification();
			}
		}
	}

	// Registration successful
	private boolean registerUser() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		Editor editor = prefs.edit();
		editor.putBoolean(Utils.REGISTRED, true);
		editor.putBoolean(Utils.FIRST_RUN, false);
		editor.commit();
		utils.startActivity(MainActivity.class, Intent.FLAG_ACTIVITY_NEW_TASK);
		return true;
	}

	private boolean restoreNotification() {
		// TODO Auto-generated method stub
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				new Intent(context, MainActivity.class),
				Intent.FLAG_ACTIVITY_NEW_TASK);
		String contentTitle = "Airtime Topup";
		String contentText = "Click to launch the main app";
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		int notificationID = 1993;
		int flags = Notification.FLAG_NO_CLEAR;
		String tickerText = "You can now quick launch airtime topup from the notification bar";
		utils.showNotification(contentIntent, contentTitle, contentText, icon,
				when, notificationID, tickerText, flags);
		return true;
	}
}
