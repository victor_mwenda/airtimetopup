package com.marvik.apps.airtime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Color;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.marvik.apps.lib.airtime.ActivityUI;
import com.marvik.apps.lib.airtime.SettingsUtils;
import com.marvik.apps.lib.airtime.Utils;

public class MainActivity extends ActivityUI {

	/*
	 * Main Activity or Home where all shit happens . . .
	 * 
	 * www.merusongs.com
	 * 
	 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
	 * 
	 * 
	 * Developed by Victor Mwenda - victor@merusongs.com
	 * 
	 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
	 * 
	 * This app enables a user to top up his/her airtime in kenya from any of
	 * the mobile networks in kenya, with advanced functionality like
	 * auto-toping up and backing up of contacts Its developed from the
	 * predecessor, Quick top up
	 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
	 */

	TextView tvTopupKeys;
	TextView tvPhonenumber;
	SharedPreferences prefs;
	String name, phone, contact;
	Dialog dialog;
	View dialogUI;
	CheckBox cbTopupOther;
	Button dialogDone;
	Button dialogCancel;
	Spinner dialogSpinner;
	AutoCompleteTextView dialogEtSearch;
	ImageView ivSearchContact;
	ArrayAdapter<String> adapter;
	SettingsUtils settingsUtils;
	Button btOtherPhone, btMyPhone;
	Utils utils;
	String sNetwork;
	String sNumber;
	String sKeys;
	String sTime;
	String sType;
	InputMethodManager inputKeyboardManager;
	TextView tvtopup;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initialise();
		insertProductKey();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		tvPhonenumber.setText("");
		tvTopupKeys.setText("");
	}

	private void initialise() {
		// TODO Auto-generated method stub

		prefs = PreferenceManager
				.getDefaultSharedPreferences(MainActivity.this);

		inputKeyboardManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

		tvtopup = (TextView) findViewById(R.id.activity_main_textview_topup);
		tvPhonenumber = (TextView) findViewById(R.id.activity_main_textView_phonenumber);
		tvPhonenumber.addTextChangedListener(new PhonenumberTextListener());
		tvPhonenumber.setOnClickListener(new PhonenumberClickListener());

		tvTopupKeys = (TextView) findViewById(R.id.activity_main_textView_secretPin);
		tvTopupKeys.addTextChangedListener(new TopupKeysTextListener());
		tvTopupKeys.setOnClickListener(new TopupKeysClickListener());

		btOtherPhone = (Button) findViewById(R.id.activity_main_button_topup_other);
		btMyPhone = (Button) findViewById(R.id.activity_main_button_topup_myphone);

		cbTopupOther = (CheckBox) findViewById(R.id.activity_main_cb_phone);

		cbTopupOther.setOnCheckedChangeListener(new TopupOtherCheckChange());
		settingsUtils = new SettingsUtils(MainActivity.this);
		utils = new Utils(MainActivity.this);
		ivSearchContact = (ImageView) findViewById(R.id.activity_main_imageView_search);

		// Disable top up of other number until the check box compound button is
		// checked
		ivSearchContact.setEnabled(false);
		tvPhonenumber.setEnabled(false);
		btOtherPhone.setEnabled(false);

		// Hide Topups if Autotopup enabled
		if (settingsUtils.autoTopup()) {
			tvtopup.setText("Auto-topup Enabled");
			tvtopup.setTextColor(Color.GREEN);
			btMyPhone.setVisibility(Button.GONE);
			btOtherPhone.setVisibility(Button.GONE);
		}
		LinearLayout keyboardLayout = (LinearLayout) findViewById(R.id.activity_main_keyboard);
		switch (settingsUtils.keyboard()) {

		case 0:
			keyboardLayout.setVisibility(LinearLayout.INVISIBLE);
			inputKeyboardManager.showSoftInput(tvPhonenumber,
					InputMethodManager.SHOW_FORCED);
			break;
		case 1:
			keyboardLayout.setVisibility(LinearLayout.VISIBLE);
			inputKeyboardManager.hideSoftInputFromWindow(
					tvTopupKeys.getWindowToken(), 0);

			break;
		}
		RelativeLayout actionBar = (RelativeLayout) findViewById(R.id.action_bar);
		if (settingsUtils.hideActionBar())
			actionBar.setVisibility(RelativeLayout.GONE);
	}

	private void insertProductKey() {
		SharedPreferences productKey = PreferenceManager
				.getDefaultSharedPreferences(MainActivity.this);
		final Editor editor = productKey.edit();
		int usageCounter = productKey.getInt(Utils.USAGE_COUNTER, 0);
		editor.putInt(Utils.USAGE_COUNTER, (usageCounter + 1));
		editor.commit();

		// First run - Allow user to quickly save his settings, navigate user
		// through whole app
		if (usageCounter == 1) {

		}

		// provide the product key to the user
		if (usageCounter == 5) {

			if (!productKey.getString(Utils.PRODUCT_KEY, "").equals(
					getString(R.string.product_key))) {
				AlertDialog.Builder alert = new AlertDialog.Builder(
						MainActivity.this);
				alert.setTitle("Product key");
				alert.setMessage("Hi "
						+ productKey.getString(Utils.USER_NAME, "")
						+ ",\n"
						+ "The app wizard noticed that you have been using Airtime Topup Pro without a product key, Do you want to insert it now?");
				alert.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								editor.putString(Utils.PRODUCT_KEY,
										getString(R.string.product_key));
								editor.commit();
								Log.i("PRODUCT KEY",
										"User inserted product key");
							}
						});
				alert.setNegativeButton("No",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								// User never accepted to fix product key
								Log.i("PRODUCT KEY", "User declined free offer");
							}
						});
				alert.create();
				alert.show();
			}
		}

		// Alert user on retopup
		if (usageCounter == 10) {

		}

		// Allow user to rate us
		if (usageCounter == 20) {

		}
		Log.i(Utils.USAGE_COUNTER, "Launch " + usageCounter);
	}

	// //////////////////////////////////////////////////
	// //// BUTTON CLICKS ///////////////
	// ////////////////

	// Top up airtime - my phone
	public void onClick_topup_me(View view) {
		topUpAirtime();
		tvTopupKeys.setText("");
	}

	private void topUpAirtime() {
		if (settingsUtils.backupTopups()) {
			backupTopup();
		}
		switch (prefs.getInt(Utils.MOBILE_NETWORK, 2)) {
		case 0:
			topup_airtel();
			break;
		case 1:
			topup_orange();
			break;
		case 2:
			topup_safaricom();
			break;
		case 3:
			topup_yu();
			break;

		}
	}

	public void onClick_searchContact(View view) {
		dialog = new Dialog(MainActivity.this);
		dialog.setTitle("Choose your email account");
		dialogUI = getLayoutInflater().inflate(R.layout.search_contact, null,
				false);
		dialogEtSearch = (AutoCompleteTextView) dialogUI
				.findViewById(R.id.search_contact_auto_tv_search);
		dialogDone = (Button) dialogUI
				.findViewById(R.id.search_contact_button_done);
		dialogDone.setOnClickListener(new DialogDoneListener());
		dialogCancel = (Button) dialogUI
				.findViewById(R.id.search_contact_button_cancel);
		dialogCancel.setOnClickListener(new DialogCancelListener());
		dialogSpinner = (Spinner) dialogUI
				.findViewById(R.id.search_contact_spinner_options);
		ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(
				MainActivity.this, android.R.layout.simple_spinner_item,
				new String[] { "Name", "Phonenumber" });
		spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		dialogSpinner.setOnItemSelectedListener(new DialogSpinnerListener());
		dialogSpinner.setAdapter(spinnerAdapter);

		readContacts();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setTitle("Search contact console");
		dialog.setContentView(dialogUI);
		dialog.show();
	}

	// Top up airtime - other phone
	public void onClick_top_up_other(View view) {
		if (tvPhonenumber.getText().length() == 10)
			topUpOtherNumber();
		tvTopupKeys.setText("");
		tvPhonenumber.setText("");
	}

	public void onClick_deleteNum(View view) throws Exception {

		if (tvPhonenumber.isFocused()) {
			if (tvPhonenumber.getText().length() != 0)
				tvPhonenumber.setText(tvPhonenumber.getText().toString()
						.substring(0, (tvPhonenumber.getText().length() - 1)));
		}
		if (tvTopupKeys.isFocused()) {
			if (tvTopupKeys.getText().length() != 0)
				tvTopupKeys.setText(tvTopupKeys.getText().toString()
						.substring(0, (tvTopupKeys.getText().length() - 1)));
		}

	}

	// Deprecated
	public void onClick_enterPin(View view) {

		tvPhonenumber.setFocusable(false);
		tvTopupKeys.setFocusable(true);
		tvTopupKeys.setCursorVisible(true);

	}

	// Deprecated
	public void onClick_enterPhone(View view) {

		tvTopupKeys.setFocusable(false);
		tvPhonenumber.setFocusable(true);
		tvPhonenumber.setCursorVisible(true);

	}

	public void onClick_num0(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("0");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("0");
	}

	public void onClick_num1(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("1");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("1");
	}

	public void onClick_num2(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("2");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("2");
	}

	public void onClick_num3(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("3");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("3");
	}

	public void onClick_num4(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("4");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("4");
	}

	public void onClick_num5(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("5");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("5");
	}

	public void onClick_num6(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("6");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("6");
	}

	public void onClick_num7(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("7");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("7");
	}

	public void onClick_num8(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("8");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("8");
	}

	public void onClick_num9(View view) {
		if (cbTopupOther.isChecked()) {
			if (tvPhonenumber.isFocused())
				tvPhonenumber.append("9");
		}
		if (tvTopupKeys.isFocused())
			tvTopupKeys.append("9");
	}

	// Top up

	// Top up Airtel
	private void topup_airtel() {
		String topupKeys = tvTopupKeys.getText().toString();
		try {
			if (topupKeys.length() == 14)
				startActivity(new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:*122*" + topupKeys + "%23")));
			else
				tvTopupKeys.setError("Invalid top up keys");
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private void topup_airtel_other() {
		String topupKeys = tvTopupKeys.getText().toString();
		String phn_no = tvPhonenumber.getText().toString();
		try {
			if (topupKeys.length() == 14 && phn_no.length() == 10)
				startActivity(new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:*122*" + topupKeys + "*" + phn_no
								+ "%23")));
			else {
				if (topupKeys.length() != 14)
					tvTopupKeys.setError("Invalid top up keys");
				if (phn_no.length() != 10)
					tvPhonenumber.setError("Invalid phonenumber");
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	// Top up Orange
	private void topup_orange() {
		String topupKeys = tvTopupKeys.getText().toString();
		try {
			if (topupKeys.length() == 12)
				startActivity(new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:*130*" + topupKeys + "%23")));
			else
				tvTopupKeys.setError("Invalid top up keys");
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private void topup_orange_other() {
		String topupKeys = tvTopupKeys.getText().toString();
		String phn_no = tvPhonenumber.getText().toString();
		try {
			if (topupKeys.length() == 12 && phn_no.length() == 10)
				startActivity(new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:*130*" + topupKeys + "*" + phn_no
								+ "%23")));
			else {
				if (topupKeys.length() != 12)
					tvTopupKeys.setError("Invalid top up keys");
				if (phn_no.length() != 10)
					tvPhonenumber.setError("Invalid phonenumber");
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	// Top up Safaricom
	private void topup_safaricom() {
		String topupKeys = tvTopupKeys.getText().toString();
		try {
			if (topupKeys.length() == 16)
				startActivity(new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:*141*" + topupKeys + "%23")));
			else
				tvTopupKeys.setError("Invalid top up keys");
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private void topup_safaricom_other() {
		String topupKeys = tvTopupKeys.getText().toString();
		String phn_no = tvPhonenumber.getText().toString();
		try {
			if (topupKeys.length() == 16 && phn_no.length() == 10)
				startActivity(new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:*141*" + topupKeys + "*" + phn_no
								+ "%23")));
			else {
				if (topupKeys.length() != 16)
					tvTopupKeys.setError("Invalid top up keys");
				if (phn_no.length() != 10)
					tvPhonenumber.setError("Invalid phonenumber");
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	// Top up YU
	private void topup_yu() {
		String topupKeys = tvTopupKeys.getText().toString();
		try {
			if (topupKeys.length() == 14)
				startActivity(new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:*130*" + topupKeys + "%23")));
			else
				tvTopupKeys.setError("Invalid top up keys");
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private void topup_yu_other() {
		String topupKeys = tvTopupKeys.getText().toString();
		String phn_no = tvPhonenumber.getText().toString();
		try {
			if (topupKeys.length() == 14 && phn_no.length() == 10)
				startActivity(new Intent(Intent.ACTION_CALL,
						Uri.parse("tel:*130*" + topupKeys + "*" + phn_no
								+ "%23")));
			else {
				if (topupKeys.length() != 14)
					tvTopupKeys.setError("Invalid top up keys");
				if (phn_no.length() != 10)
					tvPhonenumber.setError("Invalid phonenumber");
			}

		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private void topUpOtherNumber() {
		// TODO Auto-generated method stub
		if (settingsUtils.backupTopups()) {
			backupTopup();
		}
		switch (prefs.getInt(Utils.MOBILE_NETWORK, 2)) {
		case 0:
			topup_airtel_other();
			break;
		case 1:
			topup_orange_other();
			break;
		case 2:
			topup_safaricom_other();
			break;
		case 3:
			topup_yu_other();
			break;

		}

	}

	// Read contacts and set AutoComplete text view Adapter
	private void readContacts() {
		// TODO Auto-generated method stub

		// List<String> contactsName = new ArrayList<String>();
		// List<String> contactsPhones = new ArrayList<String>();
		List<String> contactsInfo = new ArrayList<String>();

		Cursor cursor = getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				new String[] { ContactsContract.CommonDataKinds.Phone.NUMBER,
						ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME },
				null, null, null);
		while (cursor.moveToNext()) {
			name = cursor
					.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			phone = cursor
					.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			contact = cursor
					.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
					+ " "
					+ cursor.getString(cursor
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

			// contactsName.add(name);
			// contactsPhones.add(phone);
			contactsInfo.add(contact);
		}

		switch (dialogSpinner.getSelectedItemPosition()) {
		case 0:
			adapter = new ArrayAdapter<String>(MainActivity.this,
					android.R.layout.simple_spinner_item, contactsInfo);
			dialogEtSearch.setAdapter(adapter);
			break;
		case 1:
			adapter = new ArrayAdapter<String>(MainActivity.this,
					android.R.layout.simple_spinner_item, contactsInfo);
			dialogEtSearch.setAdapter(adapter);
			break;
		case 2:
			adapter = new ArrayAdapter<String>(MainActivity.this,
					android.R.layout.simple_spinner_item, contactsInfo);
			dialogEtSearch.setAdapter(adapter);
			break;
		}
	}

	// Classes to handle view click events
	class DialogSpinnerListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub

			readContacts();
			switch (arg2) {
			case 0:
				dialogEtSearch.setInputType(InputType.TYPE_CLASS_TEXT);
				break;
			case 1:
				dialogEtSearch.setInputType(InputType.TYPE_CLASS_PHONE);
				break;

			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			readContacts();
		}
	}

	class TopupOtherCheckChange implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
			// TODO Auto-generated method stub
			if (arg1) {
				tvPhonenumber.setEnabled(true);
				ivSearchContact.setEnabled(true);
				btOtherPhone.setEnabled(true);
				btMyPhone.setEnabled(false);
			} else {
				tvPhonenumber.setEnabled(false);
				ivSearchContact.setEnabled(false);
				btOtherPhone.setEnabled(false);
				btMyPhone.setEnabled(true);
			}
		}

	}

	class DialogDoneListener implements OnClickListener {
		public void onClick(View view) {
			if (dialogEtSearch.getText().toString().equals("")) {
				utils.toast("No phonenumber selected", Toast.LENGTH_SHORT);
			}
			tvPhonenumber.setText(utils.formatPhonenumber(dialogEtSearch
					.getText().toString()));
			dialog.dismiss();
		}
	}

	class DialogCancelListener implements OnClickListener {
		public void onClick(View view) {
			dialog.dismiss();
		}
	}

	class PhonenumberTextListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
			// TODO Auto-generated method stub

			Log.i("PhonenumberTextListener", "Current text:"
					+ tvTopupKeys.getText().toString() + " : length:"
					+ tvTopupKeys.getText().length());
			try {
				if (settingsUtils.autoTopup())
					if (cbTopupOther.isChecked()) {
						if (tvPhonenumber.getText().length() == 10) {

							autoTopUp_other();
						}

					}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class TopupKeysTextListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			Log.i("TopupKeysTextListener", "Current text:"
					+ tvTopupKeys.getText().toString() + " : length:"
					+ tvTopupKeys.getText().length());
			try {
				if (settingsUtils.autoTopup()) {
					if (cbTopupOther.isChecked()) {
						autoTopUp_other();
					} else {

						autoTopUp();
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void autoTopUp() {
			// TODO Auto-generated method stub
			switch (tvTopupKeys.getText().length()) {
			case 12:
				switch (settingsUtils.mobileNetwork()) {

				case 1:
					if (settingsUtils.autoTopup()) {
						backupTopup();
					}
					topup_orange();

					break;
				}
				break;
			case 14:
				switch (settingsUtils.mobileNetwork()) {
				case 0:
					if (settingsUtils.autoTopup()) {
						backupTopup();
					}
					topup_airtel();
					break;

				case 3:
					topup_yu();
					break;
				}
				break;
			case 16:
				if (settingsUtils.autoTopup()) {
					backupTopup();
				}
				topup_safaricom();
				break;
			}
		}
	}

	private void autoTopUp_other() {
		if (tvPhonenumber.getText().length() == 10) {
			switch (tvTopupKeys.getText().length()) {
			case 12:
				switch (settingsUtils.mobileNetwork()) {
				case 1:
					if (settingsUtils.backupTopups()) {
						backupTopup();
					}
					topup_orange_other();
					break;
				}
				break;
			case 14:
				switch (settingsUtils.mobileNetwork()) {
				case 0:
					if (settingsUtils.backupTopups()) {
						backupTopup();
					}
					topup_airtel_other();
					break;

				case 3:
					if (settingsUtils.backupTopups()) {
						backupTopup();
					}
					topup_yu_other();
					break;
				}
				break;
			case 16:
				if (settingsUtils.backupTopups()) {
					backupTopup();
				}
				topup_safaricom_other();
				break;
			}
		}
	}

	private void backupTopup() {
		Log.i("Topups", "Backing up");
		String[] aNetwork = getResources().getStringArray(
				R.array.mobile_networks);

		sNetwork = aNetwork[settingsUtils.mobileNetwork()];
		sNumber = Utils.MY_PHONE;
		sType = Utils.TOPED_UP;
		if (cbTopupOther.isChecked()) {
			sNumber = new String(tvPhonenumber.getText().toString());
		}
		sKeys = tvTopupKeys.getText().toString();
		sTime = new Date().toString();

		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(Utils.COL_NETWORK, sNetwork);
		cv.put(Utils.COL_TIME, sTime);
		cv.put(Utils.COL_TOPUP_KEYS, sKeys);
		cv.put(Utils.COL_TOPUP_NUMBER, sNumber);
		cv.put(Utils.COL_TOPUP_TYPE, sType);
		getContentResolver().insert(Utils.CONTENT_URI, cv);

	}

	class PhonenumberClickListener implements OnClickListener {

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			boolean keyboardVisible = inputKeyboardManager.isActive();
			if (keyboardVisible) {
				LinearLayout keyboardLayout = (LinearLayout) findViewById(R.id.activity_main_keyboard);
				keyboardLayout.setVisibility(LinearLayout.INVISIBLE);
				inputKeyboardManager.showSoftInput(tvPhonenumber,
						InputMethodManager.SHOW_FORCED);
			}
		}
	}

	class TopupKeysClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			boolean keyboardVisible = inputKeyboardManager.isActive();
			if (keyboardVisible) {
				LinearLayout keyboardLayout = (LinearLayout) findViewById(R.id.activity_main_keyboard);
				keyboardLayout.setVisibility(LinearLayout.INVISIBLE);
				inputKeyboardManager.showSoftInput(tvPhonenumber,
						InputMethodManager.SHOW_FORCED);
			}
		}
	}

}
