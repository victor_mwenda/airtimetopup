package com.marvik.apps.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.webkit.WebView;

public class Test extends Activity {
	WebView webview;

	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test);
		
		try{
		NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(Test.this);
		if(!nfcAdapter.isEnabled()){
			AlertDialog.Builder dialog = new AlertDialog.Builder(Test.this, AlertDialog.THEME_HOLO_LIGHT);
			dialog.setTitle("Nfc Adapter");
			dialog.setMessage("Please enable NFC");
			dialog.create();
			dialog.show();
		
		}else{
			AlertDialog.Builder dialog = new AlertDialog.Builder(Test.this, AlertDialog.THEME_HOLO_LIGHT);
			dialog.setTitle("Nfc Adapter");
			dialog.setMessage("Your device does not have a NFC Adapter");
			dialog.create();
			dialog.show();
		}
	}catch(NullPointerException e){
		AlertDialog.Builder dialog = new AlertDialog.Builder(Test.this, AlertDialog.THEME_HOLO_LIGHT);
		dialog.setTitle("Nfc Adapter");
		dialog.setMessage(e.toString());
		dialog.create();
		dialog.show();
		}
	}
	
}
