package com.marvik.apps.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.marvik.apps.lib.airtime.Utils;

public class TopupDatabase extends SQLiteOpenHelper {

	public TopupDatabase(Context context) {
		super(context, Utils.DATABASE_NAME, null, Utils.DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE " + Utils.TABLE_NAME + "(" + Utils.COL_ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + Utils.COL_TOPUP_KEYS
				+ " TEXT, " + Utils.COL_TOPUP_NUMBER + " TEXT, "
				+ Utils.COL_TOPUP_TYPE + " TEXT, " + Utils.COL_TIME + " TEXT, "
				+ Utils.COL_NETWORK + " TEXT);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + Utils.TABLE_NAME);
		onCreate(db);
	}
}
