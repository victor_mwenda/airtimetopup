package com.marvik.apps.lib.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.marvik.apps.airtime.About;
import com.marvik.apps.airtime.BugReport;
import com.marvik.apps.airtime.MainActivity;
import com.marvik.apps.airtime.R;
import com.marvik.apps.airtime.Settings;
import com.marvik.apps.airtime.Topups;

public class ActivityUI extends Activity {
	Utils utils;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		utils = new Utils(ActivityUI.this);
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menu_about:
			utils.startActivity(About.class,
					Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			break;
		case R.id.menu_bugreport:
			utils.startActivity(BugReport.class,
					Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			break;
		case R.id.menu_exit:
			finish();
			break;
		case R.id.menu_settings:
			utils.startActivity(Settings.class,
					Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			break;
		case R.id.menu_top_ups:
			utils.startActivity(Topups.class,
					Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			break;

		}
		return super.onMenuItemSelected(featureId, item);
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////
	// //////////
	// ////////// ACTION BAR ITEMS
	// /////////////////////////////////////////////////////////////////////////////////////////////////////
	public void onClick_appIcon(View view) {
		startActivity(new Intent(getApplicationContext(), MainActivity.class)
				.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
	}

	public void onClick_appName(View view) {
		startActivity(new Intent(getApplicationContext(), About.class)
				.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
	}

	public void onClick_prefsIcon(View view) {
		startActivity(new Intent(getApplicationContext(), Settings.class)
				.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
	}

	public void onClick_exit(View view) {
		finish();
	}

}
