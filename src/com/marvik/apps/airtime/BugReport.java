package com.marvik.apps.airtime;

/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.marvik.apps.lib.airtime.ActivityUI;
import com.marvik.apps.lib.airtime.SettingsUtils;
import com.marvik.apps.lib.airtime.Utils;

public class BugReport extends ActivityUI {
	EditText etName, etEmail, etProblem;
	ProgressDialog dialog;
	int status;
	Utils utils;
	TelephonyManager tm;
	SettingsUtils settingsUtils;
	Spinner spNetwork;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bug_report);
		initialise();
		addUserEmail();

	}

	public void test(View view) {
		startActivity(new Intent(getApplicationContext(), Test.class));
	}

	private void initialise() {
		// TODO Auto-generated method stub
		utils = new Utils(BugReport.this);
		settingsUtils = new SettingsUtils(BugReport.this);
		tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		etName = (EditText) findViewById(R.id.bug_report_editText_name);
		etEmail = (EditText) findViewById(R.id.bug_report_editText_email);
		etProblem = (EditText) findViewById(R.id.bug_report_editText_bug);

		spNetwork = (Spinner) findViewById(R.id.bug_report_spinner_network);
		spNetwork.setSelection(settingsUtils.mobileNetwork());

		RelativeLayout actionBar = (RelativeLayout) findViewById(R.id.action_bar);
		if (settingsUtils.hideActionBar())
			actionBar.setVisibility(RelativeLayout.GONE);
	}

	public void onClick_sendReport(View view) {
		try {
			if (!etName.getText().toString().equals("")
					&& !etEmail.getText().toString().equals("")
					&& !etProblem.getText().toString().equals("")) {
				Pattern emailPattern = Patterns.EMAIL_ADDRESS;
				if (emailPattern.matcher(etEmail.getText().toString())
						.matches()) {
					new SendBugReport().execute();
				} else {
					etEmail.setError("Invalid email");
				}
			} else {
				if (etName.getText().toString().equals("")) {
					etName.setError("Cannot be blank");
				}
				if (etProblem.getText().toString().equals("")) {
					etProblem.setError("Cannot be blank");
				}
				if (etEmail.getText().toString().equals("")) {
					etEmail.setError("Cannot be blank");
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private class SendBugReport extends AsyncTask<Void, Void, Integer> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			dialog = new ProgressDialog(BugReport.this);
			dialog.setTitle("Bugreport console");
			dialog.setMessage("Sending bug report to developer");
			dialog.setCancelable(false);
			dialog.show();

			super.onPreExecute();
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Integer doInBackground(Void... params) {
			try {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(
						"http://www.merusongs.com/victormwenda/apps/airtime_topup/files/bugreport.php");
				List<NameValuePair> regForm = new ArrayList<NameValuePair>();
				String[] network = getResources().getStringArray(
						R.array.mobile_networks);
				regForm.add(new BasicNameValuePair("name", etName.getText()
						.toString()));
				regForm.add(new BasicNameValuePair("email", etEmail.getText()
						.toString()));
				regForm.add(new BasicNameValuePair("developer_rating", String
						.valueOf(settingsUtils.rating())));
				regForm.add(new BasicNameValuePair("sdk_ver", Build.VERSION.SDK));
				regForm.add(new BasicNameValuePair("imei", tm.getDeviceId()));
				regForm.add(new BasicNameValuePair("phone_no", tm
						.getLine1Number()));
				regForm.add(new BasicNameValuePair("network",
						network[settingsUtils.mobileNetwork()]));
				regForm.add(new BasicNameValuePair("bug", etProblem.getText()
						.toString()));

				post.setEntity(new UrlEncodedFormEntity(regForm));
				HttpResponse response = client.execute(post);
				status = response.getStatusLine().getStatusCode();
				return status;
			} catch (Exception e) {
				e.printStackTrace();
			}

			return Utils.MARVIK_HTTP_ERROR;
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub

			dialog.dismiss();
			switch (result) {
			case 200:
				sendBroadcast(new Intent(Utils.BUGREPORT_SENT));
				etEmail.setText("");
				etName.setText("");
				etProblem.setText("");
				break;
			case Utils.MARVIK_HTTP_ERROR:
				sendBroadcast(new Intent(Utils.ERROR_NO_INTERNET_ACCESS));
				break;
			default:
				sendBroadcast(new Intent(Utils.BUGREPORT_FAILED));

				break;
			}

			super.onPostExecute(result);
		}
	}

	private void addUserEmail() throws NullPointerException {
		// TODO Auto-generated method stub
		AccountManager man = (AccountManager) getSystemService(ACCOUNT_SERVICE);
		Account[] account = man.getAccounts();
		String emailAccount = "";
		Pattern emailPattern = Patterns.EMAIL_ADDRESS;
		final ArrayList<String> emailsList = new ArrayList<String>();
		for (Account mAccount : account) {

			if (emailPattern.matcher(mAccount.name).matches())
				emailsList.add(mAccount.name);

		}
		if (emailsList.size() == 0) {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(BugReport.this);
			etName.setText(prefs.getString(Utils.USER_NAME, ""));
			etEmail.setText(prefs.getString(Utils.USER_EMAIL, ""));

		}
		if (emailsList.size() == 1) {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(BugReport.this);
			etName.setText(prefs.getString(Utils.USER_NAME, ""));
			emailAccount = emailsList.get(0);
			etEmail.setText(emailAccount);
		}
		if (emailsList.size() > 1) {
			final Dialog dialog = new Dialog(BugReport.this);
			final ListView listView = new ListView(BugReport.this);
			listView.setAdapter(new ArrayAdapter<String>(BugReport.this,
					R.layout.custom_list, R.id.custom_list_textView, emailsList));
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					SharedPreferences prefs = PreferenceManager
							.getDefaultSharedPreferences(BugReport.this);
					etName.setText(prefs.getString(Utils.USER_NAME, ""));
					etEmail.setText(emailsList.get(arg2));
					dialog.dismiss();
				}
			});
			dialog.setTitle("Choose your email account");
			dialog.setContentView(listView);
			dialog.show();
			dialog.setCancelable(false);

		}
	}
}
