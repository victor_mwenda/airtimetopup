package com.marvik.apps.lib.airtime;
/*
 * Main Activity or Home where all shit happens . . .
 * 
 * www.merusongs.com
 * 
 * App download link http://www.merusongs.com/MarvikApps/AirtimeTopup.apk
 * 
 * 
 * Developed by Victor Mwenda - victor@merusongs.com
 * 
 * From Wednesday 10:00 am 30-04-2014 to 3:00 pm 3-05-2014
 * 
 * This app enables a user to top up his/her airtime in kenya from any of
 * the mobile networks in kenya, with advanced functionality like
 * auto-toping up and backing up of contacts Its developed from the
 * predecessor, Quick top up
 * http://www.merusongs.com/MarvikApps/QuickTopup.apk
 */

import java.net.URL;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("DefaultLocale")
public class Utils {
	
	//App counter
	public static final String USAGE_COUNTER="usageCounter";
	
	//Database Cursorfactory
	public static final String  TOPED_UP="toped-up";
	public static final String MY_PHONE="my_phone";
	
	//Retopup
	public static final String EXTRA_NETWORK="com.marvik.apps.airtime.EXTRA_NETWORK";
	public static final String EXTRA_TOPUP_KEYS="com.marvik.apps.airtime.EXTRA_TOPUP_KEYS";
	public static final String EXTRA_PHONENUMBER="com.marvik.apps.airtime.EXTRA_PHONENUMBER";
	
	// Marvik Apps Broadcasts
	public static final String MARVIK_APPS_EXTRA_APP_NAME = "com.marvik.apps.APP_NAME";
	public static final String MARVIK_APPS_EXTRA_DEVELOPER_NAME = "com.marvik.apps.DEVELOPER_NAME";
	public static final String MARVIK_APPS_EXTRA_DOWNLOAD_LINK = "com.marvik.apps.DOWNLOAD_LINK";
	public static final String MARVIK_APP_FIRST_RUN = "com.marvik.apps.FIRST_RUN";
	public static final String MARVIK_APPS_EXTRA_PACKAGE_NAME = "com.marvik.apps.PACKAGE_NAME";

	// Preferences
	public static final String ADD_SHORTCUT = "addShortcut";
	public static final String BACKUP_TOPUPS = "backupTopups";
	public static final String SHOW_SPLASH = "showSplash";
	public static final String SHOW_NOTIFICATION = "showNotification";
	public static final String RATING = "rating";
	public static final String KEYBOARD = "keyboard";
	public static final String MOBILE_NETWORK = "mobileNetwork";
	public static final String PRODUCT_KEY = "productKey";
	public static final String AUTO_TOPUP = "autoTopup";
	public static final String FIRST_RUN = "firstRun";
	public static final String HIDE_ACTIONBAR = "hideActionBar";
	public static final String REGISTRED = "registered";
	public static final String USER_NAME = "username";
	public static final String USER_EMAIL = "useremail";

	// Database
	public static final String DATABASE_NAME = "AirtimeTopup";
	public static final int DATABASE_VERSION = 1;
	public static final String TABLE_NAME = "Topups";

	// Database columns
	public static final String COL_ID = "_id";
	public static final String COL_TOPUP_KEYS = "keys";
	public static final String COL_TOPUP_NUMBER = "number";
	public static final String COL_NETWORK = "network";
	public static final String COL_TIME = "time";
	public static final String COL_TOPUP_TYPE = "type";

	// ContentProvider
	public static final String AUTHORIZATION = "com.marvik.apps.airtime.TopupProvider";
	public static final Uri CONTENT_URI = Uri.parse("content://"
			+ AUTHORIZATION + "/" + Utils.TABLE_NAME);

	//HTTP ERROR
	public static final int MARVIK_HTTP_ERROR = 3358;
	
	// APP custom broadcasts
	
	public static String REGISTRATION_SUCCESSFUL = "com.marvik.apps.airtime.REGISTRATION_SUCCESSFUL";
	public static String REGISTRATION_FAILED = "com.marvik.apps.airtime.REGISTRATION_FAILED";
	public static String BUGREPORT_SENT = "com.marvik.apps.airtime.BUGREPORT_SENT";
	public static final String ERROR_NO_INTERNET_ACCESS = "com.marvik.apps.airtime.ERROR_NO_INTERNET_ACCESS";
	public static final String BUGREPORT_FAILED = "com.marvik.apps.airtime.BUGREPORT_FAILED";
	public static final String SETTINGS_UPLOAD_FAILED = "com.marvik.apps.airtime.SETTINGS_UPLOAD_FAILED";
	public static final String SETTINGS_UPLOAD_SUCCESSFUL = "com.marvik.apps.airtime.SETTINGS_UPLOAD_SUCCESSFUL";
	public static final String SETTINGS_UPLOAD_FAILED_INTERNET_ERROR = "com.marvik.apps.airtime.SETTINGS_UPLOAD_FAILED_INTERNET_ERROR";
	public static final String ACTION_UPLOAD_SETTINGS = "com.marvik.apps.airtime.ACTION_UPLOAD_SETTINGS";
	// Upload Threads
	public String RETURN_STRING;
	public int RETURN_INT;

	// Context
	Context context;

	public Utils(Context context) {
		this.context = context;
	}

	public void showDialog(Dialog dialog, String message, String title) {
	}

	public void showDialog(Dialog dialog, String message, String title,
			boolean cancellable) {
	}

	public void showDialog(Dialog dialog, String message, String title,
			View view) {
	}

	public void showDialog(Dialog dialog, String message, String title,
			View view, boolean cancellable) {
	}

	public void toast(String text, int duration) {
		Toast.makeText(context, text, duration).show();
	}

	@SuppressWarnings("deprecation")
	public void showNotification(PendingIntent contentIntent,
			String contentTitle, String contentText, int icon, long when,
			int notificationID, String tickerText,int flags) {

		NotificationManager nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, tickerText, when);
		notification.setLatestEventInfo(context, contentTitle, contentText,
				contentIntent);
		notification.flags = flags;
		nm.notify(notificationID, notification);

	}

	@SuppressWarnings("deprecation")
	public void cancelNotification(PendingIntent contentIntent,
			String contentTitle, String contentText, int icon, long when,
			int notificationID, String tickerText,int flags) {

		NotificationManager nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, tickerText, when);
		notification.setLatestEventInfo(context, contentTitle, contentText,
				contentIntent);
		notification.flags = flags;
		nm.notify(notificationID, notification);
		nm.cancel(notificationID);

	}

	public void startActivity(Class<?> activity, int flags) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(context.getApplicationContext(), activity);
		intent.addFlags(flags);
		context.startActivity(intent);

	}
	public void startActivity(String activity, int flags) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(activity);
		intent.addFlags(flags);
		context.startActivity(intent);

	}

	@SuppressLint("DefaultLocale")
	public String formatPhonenumber(String phoneNumber) {
		String s = phoneNumber.toUpperCase(Locale.ENGLISH);

		// Remove all spaces and non-numbers
		s = s.replace("+254", "0");
		s = s.replace(" ", "");
		s = s.replace("A", "");
		s = s.replace("B", "");
		s = s.replace("C", "");
		s = s.replace("D", "");
		s = s.replace("E", "");
		s = s.replace("F", "");
		s = s.replace("G", "");
		s = s.replace("H", "");
		s = s.replace("I", "");
		s = s.replace("J", "");
		s = s.replace("K", "");
		s = s.replace("L", "");
		s = s.replace("M", "");
		s = s.replace("N", "");
		s = s.replace("O", "");
		s = s.replace("P", "");
		s = s.replace("Q", "");
		s = s.replace("R", "");
		s = s.replace("S", "");
		s = s.replace("T", "");
		s = s.replace("U", "");
		s = s.replace("V", "");
		s = s.replace("W", "");
		s = s.replace("X", "");
		s = s.replace("Y", "");
		s = s.replace("Z", "");
		s = s.replace("`", "");
		s = s.replace("!", "");
		s = s.replace("@", "");
		s = s.replace("#", "");
		s = s.replace("$", "");
		s = s.replace("%", "");
		s = s.replace("^", "");
		s = s.replace("&", "");
		s = s.replace("*", "");
		s = s.replace("(", "");
		s = s.replace(")", "");
		s = s.replace("_", "");
		s = s.replace("-", "");
		s = s.replace("+", "");
		s = s.replace("=", "");
		s = s.replace("{", "");
		s = s.replace("}", "");
		s = s.replace("[", "");
		s = s.replace("]", "");
		s = s.replace(";", "");
		s = s.replace(":", "");
		s = s.replace("'", "");
		s = s.replace("\"", "");
		s = s.replace("\\", "");
		s = s.replace("/", "");
		s = s.replace("?", "");
		s = s.replace("<", "");
		s = s.replace(",", "");
		s = s.replace(">", "");
		s = s.replace(".", "");
		s = s.replace("~", "");
		s = s.replace("|", "");

		return s;
	}

	public String uploadData(final URL url, final List<NameValuePair> uploadForm) {

		new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost(url.toString());
					post.setEntity(new UrlEncodedFormEntity(uploadForm));
					HttpResponse response = client.execute(post);
					RETURN_STRING = String
							.valueOf(response.getStatusLine().getStatusCode());

				} catch (Exception e) {

				}

			}

		}.run();

		return RETURN_STRING;
	}

	public int uploadData(final URL url, final List<NameValuePair> uploadForm, int result) {
		new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost(url.toString());
					post.setEntity(new UrlEncodedFormEntity(uploadForm));
					HttpResponse response = client.execute(post);
					RETURN_INT = (response.getStatusLine().getStatusCode());

				} catch (Exception e) {

				}

			}

		}.run();

		return RETURN_INT;
	}

	public void monitorPrefs() {
		// TODO Auto-generated method stub
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		if(prefs.getBoolean(Utils.FIRST_RUN, true)){
			toast("Firstrun - true", Toast.LENGTH_SHORT);
		}
		else{
			toast("Firstrun - false", Toast.LENGTH_SHORT);
		}
		
		if(prefs.getBoolean(Utils.REGISTRED, true)){
			toast("Registered - true", Toast.LENGTH_SHORT);
		}else{
			toast("Registered - false", Toast.LENGTH_SHORT);
		}
	}

	public String getString(EditText editText) {
		return editText.getText().toString();
	}
}
